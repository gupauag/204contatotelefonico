package br.com.ContatoTelefonico.Repository;

import br.com.ContatoTelefonico.Model.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    Iterable<Contato> findAllByUsuarioId(int idLogado);
}
