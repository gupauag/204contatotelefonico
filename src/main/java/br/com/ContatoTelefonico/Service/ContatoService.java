package br.com.ContatoTelefonico.Service;

import br.com.ContatoTelefonico.Model.Contato;
import br.com.ContatoTelefonico.Model.Usuario;
import br.com.ContatoTelefonico.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criaContato(String nomeContato, Usuario usuario){
        Contato contato = new Contato();
        contato.setNomeContato(nomeContato);
        contato.setUsuarioId(usuario.getId());
        return contatoRepository.save(contato);
    }
    public Iterable<Contato> getContatos(int idLogado){
        return contatoRepository.findAllByUsuarioId(idLogado);
    }


}
