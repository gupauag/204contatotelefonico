package br.com.ContatoTelefonico.Controller;

import br.com.ContatoTelefonico.Model.Contato;
import br.com.ContatoTelefonico.Model.Usuario;
import br.com.ContatoTelefonico.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping("/{nomeContato}")
    public Contato criaContato(@PathVariable(name = "nomeContato") String nomeContato,
                               @AuthenticationPrincipal Usuario usuario){
        return contatoService.criaContato(nomeContato, usuario);
    }

    @GetMapping()
    public Iterable<Contato> getContatos(@AuthenticationPrincipal Usuario usuario){
        return contatoService.getContatos(usuario.getId());
    }


}
